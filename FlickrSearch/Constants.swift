//
//  Constants.swift
//  FlickrSearch
//
//  Created by Jaime Moises Gutierrez on 1/19/17.
//  Copyright © 2017 GL. All rights reserved.
//

import UIKit

class Constants: NSObject {
	
	enum Flickr: String {
		
		case BaseURL = "https://api.flickr.com/services/rest/?method=flickr.photos.search"
		
		case Key = "01016b66310b6f47984f796fc4eee283"
		
		case Secret = "b52119e869a804f6"
		
	}
	
	enum CellIdentifier: String {
		
		case photoCollectionCell = "photoCollectionCell"
		
		
		
	}
	
}

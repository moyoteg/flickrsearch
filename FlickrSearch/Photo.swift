//
//  Photo.swift
//  FlickrSearch
//
//  Created by Jaime Moises Gutierrez on 1/19/17.
//  Copyright © 2017 GL. All rights reserved.
//

import UIKit

import UIKit


/*
Get photo example
https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
https://farm1.staticflickr.com/2/1418878_1e92283336_m.jpg
farm-id: 1
server-id: 2
photo-id: 1418878
secret: 1e92283336
size: m
*/

class Photo: NSObject {
	
	init(farmId: String, serverId: String, photoId: String, secret: String, size: Size = .medium) {
		
		self.farmId = farmId
		
		self.serverId = serverId
		
		self.photoId = photoId
		
		self.secret = secret
		
		self.size = size
		
	}
	
	var farmId: String
	
	var serverId: String
	
	var photoId: String
	
	var secret: String
	
	var size: Size
	
	enum Size: String {
		
		case large = "l"
		
		case medium = "m"
		
		case small = "s"
		
	}
	
	var defaultSizephotoURL: String {
		
		return "https://farm\(farmId).staticflickr.com/\(serverId)/\(photoId)_\(secret)_\(size).jpg"
		
	}
	
	func photoURL(size: Size) -> String {
		
		return "https://farm\(farmId).staticflickr.com/\(serverId)/\(photoId)_\(secret)_\(size).jpg"
		
	}
	
}

//
//  SearchResult.swift
//  FlickrSearch
//
//  Created by Jaime Moises Gutierrez on 1/19/17.
//  Copyright © 2017 GL. All rights reserved.
//

import UIKit

struct  SearchResult {
	
	let searchString : String
	
	let searchResults : [Photo]
	
	
}

//
//  SearchViewControllerTableViewController.swift
//  FlickrSearch
//
//  Created by Jaime Moises Gutierrez on 1/19/17.
//  Copyright © 2017 GL. All rights reserved.
//

import UIKit

class SearchViewControllerTableViewController: UITableViewController {
		
		@IBOutlet var searchTextField: UITextField!
		
		override func viewDidLoad() {
			super.viewDidLoad()
			
			
		}
		
		override func didReceiveMemoryWarning() {
			super.didReceiveMemoryWarning()
		}
		
		// MARK: - Navigation
		
		override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
			
			let alertController = UIAlertController(title: "No Text For Search!", message: "Please enter some text so that we can search for photos on flicker =).", preferredStyle: .alert)
			
			let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action) in
				print(action)
			}
			
			alertController.addAction(cancelAction)
			
			guard let text = searchTextField.text else {
				
				self.present(alertController, animated: true, completion: nil)
				
				return false
				
			}
			
			if text.characters.count <= 0 {
				
				self.present(alertController, animated: true, completion: nil)
				
				return false
				
			}
			
			return true
			
		}
		
		
		override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
			
			
			
		}

}
